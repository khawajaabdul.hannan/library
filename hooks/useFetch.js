import React, { useEffect, useState } from 'react'
import axios from 'axios';
import { useRouter } from 'next/router';


const api = (object) => {
    return axios.post(`${process.env.backEndUrl}/users/signin`, object)
}


const useFetch = (object) => {
    const [data, setData] = useState('');
    const router = useRouter()
    async function request(object) {
        if (object.email) {
            try{
            const {data: result} = await axios.post(`${process.env.backEndUrl}/users/signin`, {
                email: object.email,
                password: object.password,
            })
            localStorage.setItem('token', result.token)
            router.push('/home') 
        }
            catch(err){
            const {response}= err;
            setData(response.data)
            }
        }
    }
    useEffect(() => {
        request(object)
    }, [object])

    return [data];
}

export default useFetch