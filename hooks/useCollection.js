import axios from 'axios'
import React, { useEffect, useState } from 'react'

const useCollection = (object) => {
   const { data, setData } = useState({});

   const config = {
      headers: {
         authorization: localStorage.getItem('token')
      }
   }

   async function collections() {
      try {
         const { data: collection } = await axios.get(`${process.env.backEndUrl}/books/all`, config)
         setData(collection)
         console.log(data);
      }
      catch (err) {
         console.log(err)
      }
   }

   useEffect(() => {
      collections()
   }, [object])

   return [data];
}

export default useCollection