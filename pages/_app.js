
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

import { ThemeProvider } from "@mui/material";
import { theme } from "../utlis/theme"
import CssBaseline from '@mui/material/CssBaseline';

import { store } from '@/store/store';
import { Provider } from 'react-redux';

export default function App({ Component, pageProps }) {
  return (
      <Provider store={store}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Component {...pageProps} />
      </ThemeProvider>
      </Provider>


  )

}
